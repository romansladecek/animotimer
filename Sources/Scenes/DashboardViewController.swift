//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

final class DashboardViewController: UIViewController {
    @IBOutlet private var playButton: UIButton!
    @IBOutlet private var stopButton: UIButton!
    @IBOutlet private var bottomView: UIView!
    @IBOutlet private var timerView: UIView!
    @IBOutlet private var todayView: UIView!
    @IBOutlet private var hamburgerImageView: UIImageView!
    @IBOutlet private var hamburgerConstraintRight: NSLayoutConstraint!
    @IBOutlet private var hamburgerConstraintBottom: NSLayoutConstraint!
    @IBOutlet private var todayViewConstraintTop: NSLayoutConstraint!

    @IBOutlet private var timerTodayMinutesLabel: UILabel!
    @IBOutlet private var timerTodaySecondsLabel: UILabel!
    @IBOutlet private var timerTodayDividerView: RoundedBorderView!
    @IBOutlet private var timerTodayTitleLabel: UILabel!

    private var circleRedShape: CAShapeLayer!
    private var circleRedOriginalShape: CAShapeLayer!
    private var circleBlackShape: CAShapeLayer!
    private var circleBlackOriginalShape: CAShapeLayer!
    private var circleBlackSmallShape: CAShapeLayer!
    private var timerBackgroundShape: CAShapeLayer!
    private var playButtonShape: CAShapeLayer!
    private var playButtonOriginalShape: CAShapeLayer!
    private var stopButtonShape: CAShapeLayer!

    private var todayController: TodayViewController?

    private var timerPresented = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setupShapes()
        setupViews()
    }

    private func setupShapes() {
        circleRedShape = CAShapeLayer.circleWithCenter(center: Shapes.Center.redCircle, radius: Shapes.Radius.redCircle, color: .pinkColor)
        circleRedOriginalShape = circleRedShape
        circleBlackShape = CAShapeLayer.circleWithCenter(center: Shapes.Center.redCircle, radius: Shapes.Radius.redCircle, color: .black, opacity: 0.0)
        circleBlackOriginalShape = circleBlackShape
        circleBlackSmallShape = CAShapeLayer.circleWithCenter(center: Shapes.Center.blackSmallCircle, radius: Shapes.Radius.blackSmallCircle, color: .black, opacity: 0.0)
        timerBackgroundShape = CAShapeLayer.timerBackground()
        stopButtonShape = CAShapeLayer.stopButton(center: Shapes.Center.stopButton, size: Shapes.Size.stopButton)
        playButtonShape = CAShapeLayer.playButton(center: Shapes.Center.redCircle, size: Shapes.Size.playButton)
        playButtonOriginalShape = CAShapeLayer.playButton(center: Shapes.Center.redCircle, size: Shapes.Size.playButton)

        timerView.layer.addSublayer(circleRedShape)
        timerView.layer.addSublayer(circleBlackShape)
        timerView.layer.addSublayer(playButtonShape)
    }

    private func setupViews() {
        hamburgerConstraintRight.constant = 28
        hamburgerConstraintBottom.constant = 28
        hamburgerImageView.tintColor = .white
        setupButtons()

        timerTodayMinutesLabel.font = .timerToday
        timerTodayMinutesLabel.textColor = .pinkColor
        timerTodayMinutesLabel.text = "6"

        timerTodaySecondsLabel.font = .timerToday
        timerTodaySecondsLabel.textColor = .pinkColor
        timerTodaySecondsLabel.text = "20"

        timerTodayDividerView.backgroundColor = .pinkColor
        timerTodayDividerView.cornerRadii = CGSize(width: 7, height: 7)

        timerTodayTitleLabel.textColor = .grey50
        timerTodayTitleLabel.font = .timerTodayTitle
        timerTodayTitleLabel.attributedText = NSAttributedString(string: "Today",
                                                                 attributes:[kCTKernAttributeName as NSAttributedStringKey: 1.83])

        todayViewConstraintTop.constant = Shapes.Size.screen.height * 0.4

        displayChild(ofType: TodayViewController.self,
                     in: todayView,
                     animated: false, configuration: { controller in
                        self.todayController = controller
        }, completion: nil)
    }

    @IBAction private func timerPressed(_ sender: UIButton) {
        if timerPresented {
            animateCollapse()
            todayController?.hide()
        } else {
            animateExpand()
            todayController?.show()
        }
        timerPresented = !timerPresented
        setupButtons(disableAll: true)
    }

    private func setupButtons(disableAll: Bool = false) {
        playButton.isUserInteractionEnabled = !timerPresented && !disableAll
        stopButton.isUserInteractionEnabled = timerPresented && !disableAll
    }

    private func animateExpand() {
        circleRedShape.animateMorph(to: timerBackgroundShape, key: "morph circle to complex")
        circleBlackShape.animateMorph(to: circleBlackSmallShape, key: "morph circle black to small")
        playButtonShape.animateMorph(to: stopButtonShape, key: "morph play to stop button")
        circleBlackShape.animateOpacity(from: 0.0, to: 1.0, key: "opacity black circle")

        UIView.animate(withDuration: AnimationConstants.transitionDuration, animations: {
            self.hamburgerConstraintRight.constant = 40
            self.hamburgerConstraintBottom.constant = 60
            self.hamburgerImageView.tintColor = .black
            self.view.layoutIfNeeded()
        }) { finished in
            self.setupButtons()
        }
    }

    private func animateCollapse() {
        circleRedShape.animateMorph(to: circleRedOriginalShape, key: "morph complex to circle red")
        circleBlackShape.animateMorph(to: circleBlackOriginalShape, key: "morph circle black to original")
        playButtonShape.animateMorph(to: playButtonOriginalShape, key: "morph stop to play button")
        circleBlackShape.animateOpacity(from: 1.0, to: 0.0, key: "opacity black circle back")

        UIView.animate(withDuration: AnimationConstants.transitionDuration, animations: {
            self.hamburgerConstraintRight.constant = 28
            self.hamburgerConstraintBottom.constant = 28
            self.hamburgerImageView.tintColor = .white
            self.view.layoutIfNeeded()
        }) { finished in
            self.setupButtons()
        }
    }
}
