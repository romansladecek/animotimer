//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

final class TodayViewController: UIViewController {
    @IBOutlet private var todayQuestionLabel: UILabel!
    @IBOutlet private var todayTasksLabel: UILabel!
    @IBOutlet private var todayTasksView: UIView!
    @IBOutlet private var todayTasksViewConstraintTop: NSLayoutConstraint!
    @IBOutlet private var todayQuestionLabelConstraintTop: NSLayoutConstraint!

    @IBOutlet private var timeSeparatorView: RoundedBorderView!
    @IBOutlet private var timeCounterLabel: UILabel!
    @IBOutlet private var timeUnitLabel: UILabel!
    @IBOutlet private var timeSeparatorViewConstraintY: NSLayoutConstraint!

    private let delayCounterViews = 0.2
    private let delayTodayViews = 0.2
    private let delayTodayQuestion = 0.4

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    private func setupViews() {
        setupQuestionView()
        setupTasksView()
        setupCounterView()
    }

    private func setupQuestionView() {
        todayQuestionLabel.attributedText = NSAttributedString(string: "What are you working on?",
                                                               attributes:[kCTKernAttributeName as NSAttributedStringKey: 0.3])
        todayQuestionLabel.font = .timerTodayTask
        todayQuestionLabel.textColor = .grey30
        hideTodayQuestionViews()
    }

    private func setupTasksView() {
        todayTasksLabel.text = "+ Project & Task"
        todayTasksLabel.font = .timerTodayTask
        todayTasksLabel.textColor = .white
        todayTasksView.backgroundColor = .grey5
        todayTasksView.layer.cornerRadius = 4.0
        hideTodayTaskViews()
    }

    private func setupCounterView() {
        timeSeparatorView.backgroundColor = .grey20
        timeSeparatorView.cornerRadii = CGSize(width: 10.5, height: 10.5)
        timeSeparatorViewConstraintY.constant = 30

        timeCounterLabel.textColor = .grey20
        timeCounterLabel.font = .timerCounter
        timeUnitLabel.attributedText = NSAttributedString(string: "Sec",
                                                          attributes:[kCTKernAttributeName as NSAttributedStringKey: 1.83])
        timeUnitLabel.textColor = .grey30
        timeUnitLabel.font = .timerUnit
        hideCounterViews()
    }

    func show() {
        UIView.animate(withDuration: AnimationConstants.transitionDuration - delayTodayViews,
                       delay: delayTodayViews,
                       options: .curveEaseInOut,
                       animations: {
            self.showTodayTaskViews()
            self.view.layoutIfNeeded()
        })

        UIView.animate(withDuration: AnimationConstants.transitionDuration - delayTodayQuestion,
                       delay: delayTodayQuestion,
                       options: .curveEaseInOut,
                       animations: {
            self.showTodayQuestionViews()
            self.view.layoutIfNeeded()
        })

        UIView.animate(withDuration: AnimationConstants.transitionDuration - delayCounterViews,
                       delay: delayCounterViews,
                       options: .curveEaseInOut,
                       animations: {
                        self.showCounterViews()
                        self.view.layoutIfNeeded()
        }) { (finished) in
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           options: [.repeat, .autoreverse, .curveEaseInOut],
                           animations: {
                            self.timeSeparatorViewConstraintY.constant = -30
                            self.view.layoutIfNeeded()
            })
        }
    }

    func hide() {
        UIView.animate(withDuration: AnimationConstants.transitionDuration - delayTodayViews,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
            self.hideTodayTaskViews()
            self.view.layoutIfNeeded()
        })

        UIView.animate(withDuration: AnimationConstants.transitionDuration - delayTodayQuestion,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
            self.hideTodayQuestionViews()
            self.view.layoutIfNeeded()
        })

        UIView.animate(withDuration: AnimationConstants.transitionDuration - delayCounterViews,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.hideCounterViews()
                        self.view.layoutIfNeeded()
        })
    }

    private func showTodayTaskViews() {
        todayTasksView.alpha = 1.0
        todayTasksLabel.alpha = 1.0
        todayTasksViewConstraintTop.constant = 94.0
    }

    private func showTodayQuestionViews() {
        todayQuestionLabel.alpha = 1.0
        todayQuestionLabelConstraintTop.constant = 54.0
    }

    private func showCounterViews() {
        timeSeparatorView.alpha = 1.0
        timeCounterLabel.alpha = 1.0
        timeUnitLabel.alpha = 1.0
    }

    private func hideTodayTaskViews() {
        todayTasksView.alpha = 0.0
        todayTasksLabel.alpha = 0.0
        todayTasksViewConstraintTop.constant = 104.0
    }

    private func hideTodayQuestionViews() {
        todayQuestionLabel.alpha = 0.0
        todayQuestionLabelConstraintTop.constant = 64.0
    }

    private func hideCounterViews() {
        timeSeparatorView.alpha = 0.0
        timeCounterLabel.alpha = 0.0
        timeUnitLabel.alpha = 0.0
    }
}

extension TodayViewController: InstantiableViewController {
    static func makeInstance() -> TodayViewController {
        return R.storyboard.today.instantiateInitialViewController()!
    }
}
