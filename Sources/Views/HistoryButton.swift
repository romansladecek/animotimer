//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

final class HistoryButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func layoutSubviews(){
        super.layoutSubviews()
        layer.cornerRadius = frame.width / 2
        backgroundColor = UIColor.black
        layer.masksToBounds = true
    }
}
