//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

final class RoundedBorderView: UIView {
    public var roundedCorners: UIRectCorner = [ .allCorners ] {
        didSet { self.setNeedsLayout() }
    }

    public var cornerRadii: CGSize = CGSize(width: 7, height: 7) {
        didSet { self.setNeedsLayout() }
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        updateMask()
    }

    private func updateMask() {
        let mask = maskShapeLayer()
        mask.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: roundedCorners, cornerRadii: cornerRadii).cgPath
    }

    private func maskShapeLayer() -> CAShapeLayer {
        if let mask = layer.mask as? CAShapeLayer {
            return mask
        }
        let mask = CAShapeLayer()
        layer.mask = mask
        return mask
    }
}
