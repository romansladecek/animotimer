//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

extension UIColor {
    static let pinkColor = UIColor(red: 255/255, green: 0, blue: 79/255, alpha: 1.0)
    static let grey50 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let grey30 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    static let grey20 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    static let grey5 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
}
