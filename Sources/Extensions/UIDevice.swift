//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

extension UIDevice {
    static var safeAreaFrame: CGRect {
        if #available(iOS 11.0, *) {
            if let layoutFrame = UIApplication.shared.windows.first?.safeAreaLayoutGuide.layoutFrame {
                return layoutFrame
            }
        }
        return UIScreen.main.bounds
    }

    static var safeAreaInsets: UIEdgeInsets {
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            guard let firstWindow = UIApplication.shared.windows.first else { return insets }
            insets = firstWindow.safeAreaInsets
        }
        return insets
    }
}
