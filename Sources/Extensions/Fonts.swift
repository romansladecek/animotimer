//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

extension UIFont {
    private static let asap = "Asap"
    private static let aileron = "Aileron"
    private static let aileronSemiBold = "Aileron-SemiBold"

    @nonobjc class var timerToday: UIFont {
        return UIFont(name: asap, size: 49.0)!
    }

    @nonobjc class var timerTodayTitle: UIFont {
        return UIFont(name: aileron, size: 14.0)!
    }

    @nonobjc class var timerTodayTask: UIFont {
        return UIFont(name: aileronSemiBold, size: 18.0)!
    }

    @nonobjc class var timerCounter: UIFont {
        return UIFont(name: asap, size: 73.5)!
    }

    @nonobjc class var timerUnit: UIFont {
        return UIFont(name: aileron, size: 14)!
    }
}
