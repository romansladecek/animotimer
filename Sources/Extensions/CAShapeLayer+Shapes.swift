//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

enum Shapes {
    enum Center {
        static var redCircle: CGPoint { return CGPoint(x: 140, y: Size.screen.height - 140) }
        static var blackSmallCircle: CGPoint { return CGPoint(x: Size.screenHalf.width, y: Size.screen.height - 73) }
        static var stopButton: CGPoint { return Center.blackSmallCircle }
    }
    enum Size {
        static let statusBar = CGSize(width: UIScreen.main.bounds.width, height: 20)
        static let screen = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - statusBar.height)
        static let screenHalf = CGSize(width: Size.screen.width / 2, height: Size.screen.height / 2)
        static let playButton = CGSize(width: 34, height: 40)
        static let stopButton = CGSize(width: 16, height: 16)
    }

    enum Radius {
        static let redCircle = CGFloat(86)
        static let blackSmallCircle = CGFloat(41)
    }
}

extension CAShapeLayer {
    static func circleWithCenter(center: CGPoint, radius: CGFloat, color: UIColor, opacity: Float = 1.0) -> CAShapeLayer {
        let circlePath = UIBezierPath()
        circlePath.addArc(withCenter: center, radius: radius, startAngle: -CGFloat(Double.pi), endAngle: -CGFloat(Double.pi/2), clockwise: true)
        circlePath.addArc(withCenter: center, radius: radius, startAngle: -CGFloat(Double.pi/2), endAngle: 0, clockwise: true)
        circlePath.addArc(withCenter: center, radius: radius, startAngle: 0, endAngle: CGFloat(Double.pi/2), clockwise: true)
        circlePath.addArc(withCenter: center, radius: radius, startAngle: CGFloat(Double.pi/2), endAngle: CGFloat(Double.pi), clockwise: true)
        circlePath.close()
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = color.cgColor
        shapeLayer.opacity = opacity
        return shapeLayer
    }

    static func playButton(center: CGPoint, size: CGSize) -> CAShapeLayer {
        let path = CGMutablePath()
        let xInit = center.x - size.width / 4
        let yInit = center.y - size.height / 2
        path.move(to: CGPoint(x: xInit,
                              y: yInit))
        path.addLine(to: CGPoint(x: xInit + size.width, y: yInit + size.height/2))
        path.addLine(to: CGPoint(x: xInit, y: yInit + size.height))
        path.addLine(to: CGPoint(x: xInit, y: yInit))
        path.addLine(to: path.currentPoint)
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.white.cgColor
        layer.lineCap = kCALineCapRound
        layer.lineJoin = kCALineJoinRound
        layer.lineWidth = 3
        layer.strokeColor = UIColor.white.cgColor

        layer.path = path
        return layer
    }

    static func stopButton(center: CGPoint, size: CGSize) -> CAShapeLayer {
        let sideHalf = size.width / 2
        let path = CGMutablePath()
        let xInit = center.x - sideHalf
        let yInit = center.y - sideHalf
        path.move(to: CGPoint(x: xInit,
                              y: yInit))
        path.addLine(to: CGPoint(x: xInit + size.width, y: yInit))
        path.addLine(to: CGPoint(x: xInit + size.width, y: yInit + size.height))
        path.addLine(to: CGPoint(x: xInit, y: yInit + size.height))
        path.addLine(to: CGPoint(x: xInit, y: yInit))
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.white.cgColor
        layer.path = path
        return layer
    }

    static func timerBackground() -> CAShapeLayer {
        let height = Shapes.Size.screen.height * 0.4 // 40% od screen is filled with pink color
        let xOffset = CGFloat(80.0)
        let yOffset = Shapes.Size.screen.height + 140
        let xControlOffsetMax = CGFloat(150)
        let xControlOffsetMin = CGFloat(100)

        let path = UIBezierPath()
        path.move(to: CGPoint(x: -xOffset, y: height))
        path.addLine(to: path.currentPoint)
        path.addLine(to: path.currentPoint)
        path.addCurve(to: CGPoint(x: Shapes.Size.screen.width + xOffset, y: height),
                      controlPoint1: CGPoint(x: Shapes.Size.screenHalf.width, y: height),
                      controlPoint2: CGPoint(x: Shapes.Size.screenHalf.width, y: height))
        path.addLine(to: path.currentPoint)
        path.addCurve(to: CGPoint(x: Shapes.Size.screen.width, y: yOffset),
                      controlPoint1: CGPoint(x: Shapes.Size.screen.width + xOffset + xControlOffsetMax, y: height),
                      controlPoint2: CGPoint(x: Shapes.Size.screen.width + xOffset + xControlOffsetMin, y: yOffset))
        path.addLine(to: path.currentPoint)
        path.addCurve(to: CGPoint(x: -xOffset, y: height),
                      controlPoint1: CGPoint(x: -xOffset - xControlOffsetMin, y: yOffset),
                      controlPoint2: CGPoint(x: -xOffset - xControlOffsetMax, y: height))
        path.addLine(to: path.currentPoint)
        path.close()
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        return shapeLayer
    }
}
