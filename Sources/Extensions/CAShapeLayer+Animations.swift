//  Copyright © 2018 Roman Sladecek. All rights reserved.

import UIKit

enum AnimationConstants {
    static let transitionDuration: TimeInterval = 0.5
    static let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    static let fillMode = kCAFillModeBoth
}

extension CAShapeLayer {
    func animateOpacity(from: CGFloat, to: CGFloat, key: String) {
        let animation = CABasicAnimation(keyPath: "opacity")
        animation.fromValue = from
        animation.toValue = to
        animation.duration = AnimationConstants.transitionDuration
        animation.timingFunction = AnimationConstants.timingFunction
        animation.fillMode = AnimationConstants.fillMode
        animation.isRemovedOnCompletion = false
        add(animation, forKey: key)
    }

    func animateMorph(to: CAShapeLayer, key: String) {
        let animation = CABasicAnimation(keyPath: "path")
        animation.toValue = to.path
        animation.duration = AnimationConstants.transitionDuration
        animation.timingFunction = AnimationConstants.timingFunction
        animation.fillMode = AnimationConstants.fillMode
        animation.isRemovedOnCompletion = false
        add(animation, forKey: key)
    }
}
